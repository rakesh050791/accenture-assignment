Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post 'users' =>	'api#create_user'
  get 'print_hierarchy' =>	'api#print_hierarchy'
  put 'mark_resigned' =>	'api#mark_resigned'
  get 'salary_ratio' =>	'api#salary_ratio'
end
