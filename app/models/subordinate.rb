class Subordinate < ApplicationRecord
belongs_to :reporter, class_name: User.name
belongs_to :reportee, class_name: User.name

validate :has_proper_reporter
validate :has_proper_reportee

def has_proper_reporter
  errors.add(:reporter, ' cannot be the higest role') if reporter_id == Role.minimum('level')
end

def has_proper_reportee
  errors.add(:reportee, ' cannot be the minimum role') if reportee_id == Role.maximum('level')
end
end
