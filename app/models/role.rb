class Role < ApplicationRecord
  has_many :users, dependent: :destroy

  VALID_NAMES = ["CEO", "VP", "Director", "Manager", "SDE"]

  validates :name, inclusion: {in: VALID_NAMES}
end
