class User < ApplicationRecord

  attr_accessor :reportee_id
  belongs_to :role

  has_many :reporter_subordinates, class_name: Subordinate.name, foreign_key: :reportee_id
  has_many :reporters, through: :reporter_subordinates

  has_many :reportee_subordinates, class_name: Subordinate.name, foreign_key: :reporter_id
  has_many :reportees, through: :reportee_subordinates

  after_update :update_hierarchy

  def update_hierarchy
    self.reporter_subordinates.update_all(reportee_id: self.reportees.first.id)
  end

  def assign_reportee reportee_id
    manager_id = get_manager_id(reportee_id)
    self.reporter_subordinates.build(reportee_id: manager_id, reporter_id: self.id)
    self.save
  end

  private
  def get_manager_id id
    id.present? ? User.find_by(emp_id: id).id : User.where(role_id: Role.minimum('level')).first
  end
end
