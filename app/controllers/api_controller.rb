class ApiController < ApplicationController

  before_action :get_user, only: [:print_hierarchy, :mark_resigned]

  #Method => POST, URL: /users, Params : { "name": "Rakesh", "salary": 1000, "emp_id": 11, "rating": 5, "role_id": 4, "reportee_id": 167180 }

  def create_user
    @user = User.create(user_params)
    @user.assign_reportee(manager_id = user_params['reportee_id'])
    if @user
      render json: @user, status: :created
    else
      render json: @user.errors.messages, status: :unprocessable_entity
    end
  end

  #Method => GET , URL : /print_hierarchy?emp_id=11

  def print_hierarchy
    render json: @user.reporters, status: :ok
  end

  #Method => PUT, URL : /mark_resigned , Params : { "emp_id": 11 }

  def mark_resigned
    @user.update(resigned: true)
    render json: {message: 'user marked as resigned'}, status: :ok
  end

  #Method => GET, URL : /salary_ratio
  # TODO : need to complete this meythd, its incomplete due to time shortage.

  def salary_ratio
    total_amount = User.where(resigned: false).sum(:salary)
    users_with_max_salary = User.where(resigned: false).order("salary DESC").first(2)
    users_with_max_salary.each do |user|
      average = total_amount.to_i / user.salary.to_i
      puts average
    end
  end

  private
  def user_params
    params.permit('name', 'emp_id', 'salary', 'rating', 'role_id', 'reportee_id')
  end

  def get_user
    @user = User.find_by(emp_id: params[:emp_id])
    render json: {message: 'user not found'}, status: :not_found unless @user
  end
end
