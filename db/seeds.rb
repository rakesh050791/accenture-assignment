# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Role.create(name: 'CEO', level: 1)
Role.create(name: 'VP', level: 2)
Role.create(name: 'Director', level: 3)
Role.create(name: 'Manager', level: 4)
Role.create(name: 'SDE', level: 5)