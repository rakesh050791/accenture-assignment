class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :emp_id
      t.string :salary
      t.float :rating
      t.references :role, foreign_key: true

      t.timestamps
    end
  end
end
