class CreateSubordinates < ActiveRecord::Migration[5.1]
  def change
    create_table :subordinates do |t|
      t.integer :reportee_id
      t.integer :reporter_id

      t.timestamps
    end
  end
end
